# ScreenLocker

## Description
Simple screen locking program provides ability to lock PC.

## Installation
1. Copy ScreenLocker under /usr/bin folder
2. sudo chown root /usr/bin/ScreenLocker
3. sudo chmod u+s /usr/bin/ScreenLocker
